import { Component } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import {  ConversaWatsonPage } from './../conversa-watson/conversa-watson'; 



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 
  constructor(public navCtrl: NavController) {
  
  
  }

  autenticacao(){
    this.navCtrl.setRoot(ConversaWatsonPage);
  }
}
