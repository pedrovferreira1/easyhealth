import { Component } from '@angular/core';
import { Http, ResponseOptions } from '@angular/http';
import { Platform, IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { MediaPlugin,File } from 'ionic-native';


@IonicPage()
@Component({
  selector: 'autenticacao-voz',
  templateUrl: 'autenticacao-voz.html',
})
export class AutenticacaoVozPage {

  private _platform: Platform;
  private _fileRecord: MediaPlugin;

  private _pathFile: string = '';
  private _nameFile: string;

  private _acao: string;

  private base64str:string = '';
  private basestr:string ='';

  private audioBase63:string = "";

  private title: string = "Leitor Biometria Vocal";
  constructor(platform: Platform,
              public http: Http,
              public alertCtrl: AlertController,
              public file: File) {
        this._platform = platform;
  }

  public startRecord(): void 
  {
      this._pathFile = this.getPathFileRecordAudio();
      this._fileRecord = new MediaPlugin(this._pathFile);
      this._acao = "Recording";
      this._fileRecord.startRecord();
  }

  public stopRecord(): void 
  {
        this._acao = "StopRecording";
        this._fileRecord.stopRecord();
  }

  private startPlay(): void 
  {      
        this._acao = "Play";
        this._fileRecord = new MediaPlugin(this._pathFile);
        this._fileRecord.play();
 };

 getFileContentAsBase64(){
       let path:string = "file:///" + this._pathFile;            
       
       
    }

  private getPathFileRecordAudio(): string 
  {
    let path: string = (this._platform.is('ios') ? '../Library/NoCloud/': '../Documents/');
    return path + this._nameFile + '-' + '.wav';
  }

  showModal(file:string){

  let alert = this.alertCtrl.create({
                  title: 'New Friend!',
                  subTitle: file,
                  buttons: ['OK']
                });

            alert.present();
  }

}
