import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutenticacaoVozPage } from './autenticacao-voz';
import { Base64 } from '@ionic-native/base64';

@NgModule({
  declarations: [
    AutenticacaoVozPage,
  ],
  imports: [
    IonicPageModule.forChild(AutenticacaoVozPage),
  ],
   providers: [
    Base64
   ]
})
export class AutenticacaoVozPageModule {}