import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Condominio } from "../condominio";
import { CondominioService } from "../condominio.service";
import { ListaCondominioPage } from "../lista-condominio/lista-condominio";


@IonicPage()
@Component({
  selector: 'page-cadastro-condominio',
  templateUrl: 'cadastro-condominio.html',
})
export class CadastroCondominioPage {

  private condominio:Condominio;
  private title: string;
  private tipoAcao:string = "cadastro";
  private textoAcao:string = "Cadastrar";

  constructor(public navCtrl: NavController, public navParams: NavParams, private condominioService: CondominioService) {
    this.condominio = new Condominio();
    this.title = "Cadastro de Condomínio";

    if(this.navParams.get('condominio')){
      
      this.condominioService
            .selecionar(this.navParams.get('condominio').condominioid)
            .subscribe(result=>{
              if(result)
                this.condominio = result; 
            });

      this.title = "Edição de Condomínio"

      this.tipoAcao = "edicao";
      this.textoAcao = "Editar";
    }
  }

  realizarAcao(){
    if(this.tipoAcao == "cadastro"){
      this.cadastrar();
    }
    if(this.tipoAcao == "edicao"){
      this.editar();
    }
  }
  cadastrar(){
    this.condominioService
        .cadastrar(this.condominio)
        .subscribe(result => {
          this.navCtrl.setRoot(ListaCondominioPage);
        });
  }
  editar(){
    let condId = this.condominio.condominioid;
    this.condominio.condominioid = 0;

    this.condominioService
        .atualizar(condId,this.condominio)
        .subscribe(result => {
          debugger;
          this.navCtrl.setRoot(ListaCondominioPage);
        });
  }
}
