import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroCondominioPage } from './cadastro-condominio';

@NgModule({
  declarations: [
    CadastroCondominioPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroCondominioPage),
  ],
})
export class CadastroCondominioPageModule {}
