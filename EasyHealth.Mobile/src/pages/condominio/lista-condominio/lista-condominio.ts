import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController  } from 'ionic-angular';
import { CondominioService } from './../condominio.service';
import 'rxjs/add/operator/map';
import { CadastroCondominioPage } from './../cadastro-condominio/cadastro-condominio';


@IonicPage()
@Component({
  selector: 'page-lista-condominio',
  templateUrl: 'lista-condominio.html',
})
export class ListaCondominioPage {

 private condominios: any = [ ];

  constructor(
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController, 
    private condominioService:CondominioService,
    public alertCtrl: AlertController
    ) {

    this.fetchContent();
  
  }
   fetchContent ():void {
    let loading = this.loadingCtrl.create({
      content: 'Buscando Condominios'
    });

    loading.present();
     this.condominioService
        .listar()
        .subscribe(data => {
            this.condominios = data;
            loading.dismiss();
          }); 
  }

  doRefresh(refresher) {

    this.condominioService
        .listar()
        .subscribe(data => {
            this.condominios = data;
            console.log(this.condominios);
            refresher.complete();
          }); 
  } 
  abrirOpcoes(event,condominio){
      let prompt = this.alertCtrl.create({
      title: 'Oque deseja fazer',
      buttons: [
        {
          text: 'Alterar',
          handler: data => {
            this.condominioSelecionado(event,condominio);
          }
        },
        {
          text: 'Deletar',
          handler: data => {
            this.excluir(event,condominio);
          }
        }
      ]
    });
    prompt.present();
  }
  condominioSelecionado (event,condominio):void {
    this.navCtrl.push(CadastroCondominioPage, {
      condominio: condominio
    });
  } 

  cadastrar():void{
    this.navCtrl.push(CadastroCondominioPage);
  }
  
  excluir(event,condominio){
    console.log(condominio);
    this.condominioService
        .deletar(condominio.condominioid)
        .subscribe(data => {
            this.condominioService
              .listar()
              .subscribe(data => {
                  this.condominios = data;
                }); 
          }); 
  }

}
