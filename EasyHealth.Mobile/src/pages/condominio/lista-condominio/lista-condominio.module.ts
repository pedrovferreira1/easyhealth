import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaCondominioPage } from './lista-condominio';

@NgModule({
  declarations: [
    ListaCondominioPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaCondominioPage),
  ],
})
export class ListaCondominioPageModule {}
