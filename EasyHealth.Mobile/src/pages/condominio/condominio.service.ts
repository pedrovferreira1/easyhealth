import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Condominio } from "./condominio";

@Injectable()
export class CondominioService {

    private url:string = "http://34.210.159.211/api/condominio";

    

    private options: RequestOptions;

    constructor(
        public http: Http
    ){
        let headers: Headers = new Headers();
        headers.append("Content-Type", "application/json");
        
        this.options = new RequestOptions({headers:headers});
    }
    listar() {
        return this.http
          .get(this.url)
          .map(res => res.json()); 
    }
    selecionar(condominioId: number){
        let urlGet: string = this.url + "/" +  condominioId;
        
        return this.http
            .get(urlGet)
            .map(res => res.json()); 
    }
    cadastrar( condominio: Condominio ){
     
        return this.http
            .post(
                this.url,
                JSON.stringify(condominio),
                this.options
            );
    }
    atualizar(condominioId: number, condominio: Condominio ){
        let urlGet: string = this.url + "/" +  condominioId;
        return this.http
            .patch(
                urlGet,
                JSON.stringify(condominio),
                this.options
            );
    }
    deletar( condominioId: number ){
        let urlGet: string = this.url + "/" +  condominioId;
    
        return this.http
            .delete(
                urlGet,
                this.options
            );
    }
}