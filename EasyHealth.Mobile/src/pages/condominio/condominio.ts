export class Condominio {
    condominioid: number;
    cnpj: string;
    cep: string;
    nome: string;
    estado: string;
    cidade: string;
    bairro: string;
    rua: string;
    numero: number;
    complemento: string;
    status: string;        
}