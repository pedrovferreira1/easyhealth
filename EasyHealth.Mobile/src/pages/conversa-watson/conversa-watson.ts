import { Component } from '@angular/core';
import { Http, ResponseOptions } from '@angular/http';
import { Platform, IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { MediaPlugin,File } from 'ionic-native';


@IonicPage()
@Component({
  selector: 'conversa-watson',
  templateUrl: 'conversa-watson.html',
})
export class ConversaWatsonPage {

  private messageWatsonBody:any = 
    {
        input:   
            {
                text:"start"
            },
        workspace_id:"7001a564-039c-46c2-b935-9c1147d4213a",
        context:
        {
            tenantId:"PG",
            user:"João Eduardo",
            userId:"12345678909",
            sessionId:"0123443210"
        },
        inicializa:"1"
    };  
  private title: string = "Conversa Watson";
  constructor(platform: Platform,
              public http: Http) {
  }


iniciaConversa(){
    debugger;
    this.http
        .post("https://av1nodemiddleware.mybluemix.net/conversa",this.messageWatsonBody)
        .subscribe((result:any)=>{
            console.log(result);
        });
}

}
