import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConversaWatsonPage } from './conversa-watson';
import { Base64 } from '@ionic-native/base64';

@NgModule({
  declarations: [
    ConversaWatsonPage,
  ],
  imports: [
    IonicPageModule.forChild(ConversaWatsonPage),
  ],
   providers: [
    Base64
   ]
})
export class ConversaWatsonPageModule {}