import { Directive, ElementRef, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import {Gesture} from 'ionic-angular/gestures/gesture';

@Directive({
  selector: '[longPress]'
})
export class PressDirective implements OnInit, OnDestroy {
  el: HTMLElement;
  pressGesture: Gesture;
  @Output() eventEmmiter: EventEmitter<any> = new EventEmitter();

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }

  ngOnInit() {
    
  }
  longPressEvent(){
    this.pressGesture = new Gesture(this.el);
    this.pressGesture.listen();
    this.pressGesture.on('press', e => {
      console.log("teste");
      this.eventEmmiter.emit(event);
    });
  }

  ngOnDestroy() {
    this.pressGesture.destroy();
  }
}