import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListaCondominioPage } from '../pages/condominio/lista-condominio/lista-condominio';
import { CadastroCondominioPage } from '../pages/condominio/cadastro-condominio/cadastro-condominio';

import { CondominioService } from '../pages/condominio/condominio.service';

import { AutenticacaoVozPage } from '../pages/autenticacao-voz/autenticacao-voz';
import { AutenticacaoVozPageModule } from '../pages/autenticacao-voz/autenticacao-voz.module';

import { ConversaWatsonPage } from '../pages/conversa-watson/conversa-watson';
import { ConversaWatsonPageModule } from '../pages/conversa-watson/conversa-watson.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Base64 } from '@ionic-native/base64';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListaCondominioPage,
    CadastroCondominioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AutenticacaoVozPageModule,
    ConversaWatsonPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListaCondominioPage,
    CadastroCondominioPage,
    AutenticacaoVozPage,
    ConversaWatsonPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CondominioService,
    Base64,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
