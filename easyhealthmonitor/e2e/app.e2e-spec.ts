import { EasyhealthmonitorPage } from './app.po';

describe('easyhealthmonitor App', function() {
  let page: EasyhealthmonitorPage;

  beforeEach(() => {
    page = new EasyhealthmonitorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
