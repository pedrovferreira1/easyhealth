import { Headers,Http } from '@angular/http';
import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { Router } from "@angular/router";

@Component({
    selector: 'monitoracoes-pacientes',
    templateUrl: './monitoracoes-pacientes.component.html'
})
export class MonitoracoesPacientesComponent implements OnInit {

    public pacientes:any=[];
    constructor(private http:Http,private cdRef: ChangeDetectorRef) {

        
    }
    
    getPacientes(){
        return this.http.get("http://34.215.138.252:8080/easyhealth/patients")
         .toPromise()
         .then((resp)=>{
                    console.log(resp.json());
                    this.pacientes = resp.json();
                    this.cdRef.detectChanges();
                    
                    setTimeout(()=>{ 
                        this.getPacientes();
                    },3000);


                    this.getPacientes();
            }).catch(error=>{console.log(error)});;
    }
    ngOnInit(){
     
     this.getPacientes();  
    }
}
