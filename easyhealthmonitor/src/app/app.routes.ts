import{ RouterModule,Routes } from '@angular/router';
import{ MonitoracoesPacientesComponent } from './monitoracoes-pacientes/monitoracoes-pacientes.component';
import{ AppComponent } from './app.component';

const appRoutes: Routes = [
    { path: '',component: MonitoracoesPacientesComponent },
    { path: 'monitoracoes', component: MonitoracoesPacientesComponent },
    { path: '**', component: AppComponent }
];

export const routing = RouterModule.forRoot(appRoutes);